import React from 'react';
import { Link } from 'react-router-dom';

export default class Footer extends React.Component{
    render(){
        return(
            <section className="footer" id="footer">
                <div className="container">
                    <div className="footer-left">
                        <p className="footer-title">
                            Family Optometric Vision Care
                        </p>
                        <p className="footer-address-phone footer-content">
                        5163 Lone Tree Way, Antioch, CA 94531<br/>925-757-5560
                        </p>
                        <div className="hours">
                            <p className="days">
                                Monday<br/>Tuesday<br/>Wednesday<br/>Thursday<br/>Friday<br/>Saturday<br/>Sunday<br/>
                            </p>
                            <p className="day-hours">
                                9:00AM - 6:00PM<br/>9:00AM - 6:00PM<br/>9:00AM - 5:00PM<br/>9:00AM - 5:00PM<br/>9:00AM - 5:00PM<br/>8:00AM - 2:00PM<br/>CLOSED<br/>
                            </p>
                        </div>
                        <div className="hours">
                            <p className="daysweclosed">
                                More info including special dates closed at <Link bold="true" to="/hours-location" style={{color:"#BBB"}}>Hours & Location</Link>
                            </p>
                        </div>






                        <div className="footer-social">
                            <span className="icon"><a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/familyoptometric/"><i className="fab fa-facebook-square"></i></a></span>
                            <span className="icon"><a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/familyoptovc/"><i className="fab fa-instagram"></i></a></span>
                            <span className="icon"><a target="_blank" rel="noopener noreferrer" href="https://www.yelp.com/biz/family-optometric-vision-care-antioch"><i className="fab fa-yelp"></i></a></span>
                        </div>






                        <p className="copyright">
                        Copyright © 2018 Family Optometric Vision Care. <Link to="/privacy-policy">Privacy Policy</Link>. Sitemap. Site by <a target="_blank" rel="noopener noreferrer" href="http://www.wrlowe.com">wrlowe.com</a>.
                        </p>
                    </div>
                    <div className="footer-right">
                        <Link to="/">Home</Link><br/>
                        <Link to="/">Our Services</Link><br/>
                        <Link to="/">Our Office</Link><br/>
                        <Link to="/team">Our Staff</Link><br/>
                        <Link to="eyeglasses-contacts">Eyeglasses & Contacts</Link><br/>
                        <Link to="insurance">Insurance</Link><br/>
                        <Link to="hours-location">Hours & Location</Link><br/>
                        <Link to="contact-us">Contact Us</Link><br/>
                    </div>
                </div>
            </section>
        )
    }
}